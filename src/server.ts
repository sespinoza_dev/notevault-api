import express from 'express';
import pool from './db';
import PageModel from './models/page.model';

const app = express();
const port = 4000;

const startServer = () => {
  app.get('/', async (req, res) => {
    try {
      const pageModel = new PageModel(pool);
      const pages = await pageModel.getAllPages();
      console.log('pages: ', pages);
      res.status(200).json(pages);
    } catch (err) {
      console.error(err);
      res
        .status(500)
        .json({ message: 'something went wrong, check the logs.' });
    }
  });

  app.listen(port, () => {
    return console.log(`Express is listening at http://localhost:${port}`);
  });
};

export default startServer;
