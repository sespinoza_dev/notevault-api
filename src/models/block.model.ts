import { Pool } from 'pg';
import IBlock from './block.interface';

export default class BlockModel {
  constructor(private pool: Pool) {}

  async createBlock(block: IBlock): Promise<IBlock> {
    const query = {
      text: 'INSERT INTO blocks (type, page_id, content, text) VALUES ($1, $2, $3, $4) RETURNING *',
      values: [block.type, block.page_id, block.content, block.text],
    };

    try {
      const res = await this.pool.query(query);
      return res.rows[0];
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async getBlockById(id: number): Promise<IBlock | null> {
    const query = {
      text: 'SELECT * FROM blocks WHERE id = $1',
      values: [id],
    };

    try {
      const res = await this.pool.query(query);
      return res.rows[0] || null;
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async getAllBlocks(): Promise<IBlock[]> {
    const query = {
      text: 'SELECT * FROM blocks',
    };

    try {
      const res = await this.pool.query(query);
      return res.rows;
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async updateBlock(id: number, updates: Partial<IBlock>): Promise<IBlock> {
    const query = {
      text: 'UPDATE blocks SET type = $1, page_id = $2, content = $3, text = $4 WHERE id = $5 RETURNING *',
      values: [
        updates.type,
        updates.page_id,
        updates.content,
        updates.text,
        id,
      ],
    };

    try {
      const res = await this.pool.query(query);
      return res.rows[0];
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async deleteBlock(id: number): Promise<void> {
    const query = {
      text: 'DELETE FROM blocks WHERE id = $1',
      values: [id],
    };

    try {
      await this.pool.query(query);
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async getBlocksForPage(pageId: number): Promise<IBlock[]> {
    const query = {
      text: 'SELECT * FROM blocks WHERE page_id = $1',
      values: [pageId],
    };

    try {
      const res = await this.pool.query(query);
      return res.rows;
    } catch (err) {
      console.error(err);
      throw err;
    }
  }
}
