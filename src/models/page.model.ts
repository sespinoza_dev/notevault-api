import { Pool } from 'pg';
import IPage from './page.interface';
import IBlock from './block.interface';

export default class PageModel {
  constructor(private pool: Pool) {}

  async createPage(page: IPage): Promise<IPage> {
    const query = {
      text: 'INSERT INTO pages (title) VALUES ($1) RETURNING *',
      values: [page.title],
    };

    try {
      const res = await this.pool.query(query);
      return res.rows[0];
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async getPageById(id: number): Promise<IPage | null> {
    const query = {
      text: 'SELECT * FROM pages WHERE id = $1',
      values: [id],
    };

    try {
      const res = await this.pool.query(query);
      return res.rows[0] || null;
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async getAllPages(): Promise<IPage[]> {
    const query = {
      text: 'SELECT * FROM pages',
    };

    try {
      const res = await this.pool.query(query);
      return res.rows;
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async updatePage(id: number, updates: Partial<IPage>): Promise<IPage> {
    const query = {
      text: 'UPDATE pages SET title = $1, updated_at = NOW() WHERE id = $2 RETURNING *',
      values: [updates.title, id],
    };

    try {
      const res = await this.pool.query(query);
      return res.rows[0];
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async deletePage(id: number): Promise<void> {
    const query = {
      text: 'DELETE FROM pages WHERE id = $1',
      values: [id],
    };

    try {
      await this.pool.query(query);
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  async getBlocksForPage(pageId: number): Promise<IBlock[]> {
    const query = {
      text: 'SELECT * FROM blocks WHERE page_id = $1',
      values: [pageId],
    };

    try {
      const res = await this.pool.query(query);
      return res.rows;
    } catch (err) {
      console.error(err);
      throw err;
    }
  }
}
