export default interface IPage {
  id: number;
  title: string;
  created_at: Date;
  updated_at: Date;
}
