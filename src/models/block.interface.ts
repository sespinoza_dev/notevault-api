export default interface IBlock {
  id: number;
  type: string;
  page_id: number;
  content: string;
  text: string;
  created_at: Date;
  updated_at: Date;
}
